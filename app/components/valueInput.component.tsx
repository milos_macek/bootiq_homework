import React from 'react';
import { StyleSheet, View, Text } from "react-native";
import { PickedValue } from './pickedValue.component';
import { SliderComponent } from './slider.component';
import { useStore } from '../store/store';

export const ValueInput = (props: any) => {
    const { title, type, unit, separator } = props;

    const handleSetValue = (value: number) => {
        useStore.getState().fetchLoan(type, value);
    }

    return (
        <View style={[styles.container, separator && styles.separator]} >
            <View style={styles.topContainer}>
                <Text style={styles.title}>{title}</Text>
                <PickedValue
                    type={type}
                    unit={unit}
                    onChangeValue={handleSetValue} />
            </View>
            <View style={styles.bottomContainer}>
                <SliderComponent
                    type={type}
                    onChangeValue={handleSetValue} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        flexDirection: "column",
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    separator: {
        borderBottomWidth: 2,
        borderBottomColor: "#ffffff"
    },
    topContainer: {
        flex: 2,
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    bottomContainer: {
        flex: 3,
        width: "100%",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    title: {
        flex: 3,
        fontSize: 20,
        color: "#ffffff",
        fontWeight: "bold"
    }
});
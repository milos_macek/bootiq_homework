import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { useStore } from '../store/store';

export const PickedValue = ({ type, unit }: any) => {
    const { value, defaultValue } = useStore((state: any) => (type === "amount" ? state.amountInterval : state.termInterval))

    const handeShowPicker = () => {
        console.log("Click");
        useStore.getState().togglePicker(type, true);
    }

    return (
        <TouchableOpacity style={styles.container} onPress={handeShowPicker} >
            <Text style={styles.value}>{value || defaultValue}</Text>
            <Text style={styles.unit}>{unit}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: "#eee",
        backgroundColor: "#fff",
        padding: 10,
        marginLeft: 20
    },
    value: {
        flex: 1,
        fontSize: 14,
        color: "#000",
        fontWeight: "bold"
    },
    unit: {
        fontSize: 14,
        color: "#555"
    }
});
import React from 'react';
import { StyleSheet } from "react-native";
import { useStore } from '../store/store';
import { Picker } from '@react-native-picker/picker';

export const PickerComponent = ({ type }: any) => {
    const { value, defaultValue } = useStore((state: any) => (type === "amount" ? state.amountInterval : state.termInterval))
    const values: number[] = useStore((state: any) => {
        const { min, max, step, defaultValue } = (type === "amount" ? state.amountInterval : state.termInterval);
        let values = [];
        for (let i = min; i <= max; i += step) {
            values.push(i);
        }
        return values;
    });

    const handleSetValue = (value: number) => {
        useStore.getState().togglePicker("", false);
        useStore.getState().fetchLoan(type, value);

    }

    return (
        <Picker
            selectedValue={value}
            onValueChange={(itemValue, itemIndex) => handleSetValue(itemValue)}>
            {values.map((value: number) => <Picker.Item label={value.toString()} value={value} />)}
        </Picker>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
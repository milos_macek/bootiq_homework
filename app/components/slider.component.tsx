import React from 'react';
import { Slider } from 'react-native-range-slider-expo';
import { StyleSheet, View } from "react-native";
import { useStore } from '../store/store';

export const SliderComponent = ({ type, onChangeValue }: any) => {
    const { min, max, step, defaultValue, value } = useStore((state: any) => (type === "amount" ? state.amountInterval : state.termInterval))

    return (
        <View style={styles.container}>
            <Slider min={min} max={max} step={step}
                valueOnChange={onChangeValue}
                initialValue={value || defaultValue}
                knobColor='#ffffff'
                valueLabelsBackgroundColor='black'
                inRangeBarColor='#C1C1C1'
                outOfRangeBarColor='#125421'
                rangeLabelsTextColor="#ffffff"
                showValueLabels={false}
                styleSize="medium"
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    }
})
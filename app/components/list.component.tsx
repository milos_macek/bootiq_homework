import React from 'react';
import moment from "moment";
import { StyleSheet, ScrollView, View, ActivityIndicator } from "react-native";
import { useStore } from '../store/store';
import { ListItem } from './listItem.component';

export const ListComponent = () => {
    const today = new Date();
    const { totalPrincipal, term, totalCostOfCredit, totalRepayableAmount, monthlyPayment, loading } = useStore((state: any) => state);

    return loading ? (
        <View style={styles.loading}>
            <ActivityIndicator size='large' />
        </View>
    ) : (
        <ScrollView>
            <ListItem title="Loan" value={totalPrincipal} startUnit="$ " endUnit=" MXN" />
            <ListItem title="Term" value={term} endUnit=" days" />
            <ListItem title="Total cost credit" value={totalCostOfCredit} startUnit="$ " endUnit=" MXN" />
            <ListItem title="Monthly payment" value={Math.round((monthlyPayment * 100) / 100)} startUnit="$ " endUnit=" MXN" />
            <ListItem title="Amount payable" value={totalRepayableAmount} bold startUnit="$ " endUnit=" MXN" />
            <ListItem title="Payment date" value={moment(today).add(5, 'days').format("DD/MMM/YYYY")} bold />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
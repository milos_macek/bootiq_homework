import create from "zustand";
import { persist, devtools } from "zustand/middleware";
import axios from 'axios';
import { AppConfig } from "../config/app.config";

export const useStore = create(
    devtools((set: any) => ({
        amountInterval: false,
        termInterval: false,
        error: "",
        loading: false,
        showPicker: false,
        pickerType: "",
        togglePicker: (type: string, show: boolean) => set((state: any) => ({ pickerType: type, showPicker: show })),
        fetchSettings: async () => {
            let data: any = false, error: string = "";

            set((state: any) => ({ loading: true }));

            try {
                const response = await axios(AppConfig.apiConfigUrl);

                if (response.status === 200 && response.data) {
                    set((state: any) => ({
                        amountInterval: {
                            ...response.data.amountInterval,
                            value: response.data.amountInterval.defaultValue
                        },
                        termInterval: {
                            ...response.data.termInterval,
                            value: response.data.termInterval.defaultValue
                        }
                    }));
                } else {
                    error = "Server error";
                }
            } catch (error) {
                error = error;
            } finally {
                set((state: any) => ({
                    loading: false,
                    error: error
                }));
            }
        },
        fetchLoan: async (type: string, value: number) => {
            let data: any = false, error: string = "";

            set((state: any) => {
                data = {
                    ...state,
                };
                type === "amount" ? data.amountInterval.value = value : data.termInterval.value = value;
                return data;
            });

            const url = AppConfig.apiCalculateUrl
                + "?amount=" + data.amountInterval.value
                + "&term=" + data.termInterval.value;

            try {
                const response = await axios(url);

                if (response.status === 200 && response.data) {
                    data = { ...data, ...response.data };
                } else {
                    error = "Server error";
                }
            } catch (error) {
                error = error;
            }

            set((state: any) => ({
                ...data,
                error: error
            }));
        }
    })))
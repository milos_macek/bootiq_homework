import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import LoanScreen from './app/screens/loadCalculator.screen';

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <LoanScreen />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});

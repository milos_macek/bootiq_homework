import React, { useEffect } from 'react';
import {
    StyleSheet,
    View,
    ActivityIndicator
} from 'react-native';
import { ListComponent } from '../components/list.component';
import { ValueInput } from '../components/valueInput.component';
import { useStore } from '../store/store';
import { PickerComponent } from '../components/picker.component';

export default function LoanScreen() {
    const { loading, fetchSettings, showPicker, pickerType } = useStore((state: any) => state);

    useEffect(() => {
        fetchSettings();
    }, []);


    return (
        <View style={styles.container}>
            <View style={styles.inputsContainer}>
                {loading ? (
                    <View style={styles.loading}>
                        <ActivityIndicator size='large' />
                    </View>
                ) : (
                    <>
                        <ValueInput type="amount" title="Total amount" unit="$" separator />
                        <ValueInput type="term" title="Term" unit="days" />
                    </>
                )}
            </View>
            <View style={styles.outputsContainer}>
                {showPicker ? <PickerComponent type={pickerType} /> : <ListComponent />}
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: 'flex-start'
    },
    inputsContainer: {
        flex: 3,
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: '#94C62D',
        paddingTop: 50,
        paddingHorizontal: 20
    },
    outputsContainer: {
        flex: 4,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
import React from 'react';
import { StyleSheet, View, Text } from "react-native";

export const ListItem = ({ title, value, bold, startUnit, endUnit }: any) => {

    return (
        <View style={styles.container} >
            <Text style={[styles.title, bold && { fontWeight: "bold" }]}>{title}</Text>
            <Text style={[styles.value, bold && { fontWeight: "bold" }]}>
                {startUnit && <Text>{startUnit}</Text>}
                <Text>{value}</Text>
                {endUnit && <Text>{endUnit}</Text>}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
        paddingVertical: 20,
        borderBottomWidth: 2,
        borderBottomColor: "#606060"
    },
    title: {
        flex: 1,
        fontSize: 14,
        color: "#000",
    },
    value: {
        fontSize: 14,
        color: "#555"
    }
});